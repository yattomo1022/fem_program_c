#ifndef vtkOutput_class_h
#define vtkOutput_class_h

#include "geometry_class.hpp"
#include <iostream>
#include <fstream>
#include <string>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
//this is static class
class vtkOutput_class
{
private:
    vtkOutput_class(); //実装は書かない
    static const int _scalar;
    static const int _vector;

public:
    static void vtkOutput_header(ofstream &, string, int, int); //nnode, nelem;
    //static void vtkOutput_pdataheader(ofstream &, const arma::Col<int> &,
    //                                  const arma::field<std::string> &); //
    static void vtkOutput_pointdata(ofstream &, const arma::Col<int> &, const arma::field<std::string> &,
                                    arma::Mat<double> *ptr[]); //datatype, dataname, pointdata
    //static void vktOutput_pdatafooter(ofstream &);

    static void vtkOutput_point(ofstream &, const geometry_class &, const arma::Col<int> &);
    static void vtkOutput_cell(ofstream &, const geometry_class &, const arma::Col<int> &, arma::Col<int> &);
    static void vktOutput_footer(ofstream &);
};

#endif