#ifndef main_h
#define main_h
#include <string>
#include <complex>
#define complex std::complex<double>

extern std::string gmshdir;
extern std::string vtkdir;
extern const double PI;
extern const complex ci;
#endif