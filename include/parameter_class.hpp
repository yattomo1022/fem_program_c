#ifndef parameter_class_h
#define parameter_class_h

#include "geometry_class.hpp"
#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <memory>

class parameter_class
{
private:
  struct air_type
  {
  public:
    bool is_set = false; // set flag
    double P0;           //[Pa]
    double rho0;         //[kg/m^3]
    double c0;           //[m/s]
    double Z_char;       // Characteristic Impedance
    double prandtl;      //プランドル定数
    double SHR;          //比熱比 []
    double viscosity;
    double c_T;
    double c_V;
  };

  arma::Col<int> param_conv;
  arma::field<air_type> air_param;

public:
  //initialize
  void init(const geometry_class &);
  //残りをデフォルト値で埋める
  void set_default();
  //in: domainID(1based array) out: void
  void set_air_param(int, double temp = 20.0, bool flag = true);
  //in: domainID(1based array) out: wave speed
  double get_air_c0(int) const;
};

#endif