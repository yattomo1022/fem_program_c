#ifndef utility_tool_class_h
#define utility_tool_class_h

#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

class utility_tool_class
{
  private:
    utility_tool_class(); //定義しない

  public:
    static char *StrToLower(char *s);
    static std::tuple<int, bool> binary_search(int, const long long *, const long long *);
};

//extern utility_tool_class tools;

#endif
