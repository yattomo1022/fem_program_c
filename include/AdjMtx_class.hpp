#ifndef AdjMtx_class_h
#define AdjMtx_class_h

#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <memory>
#include "tree_class.hpp"
#include "geometry_class.hpp"

class SpMtx_core
{
public:
  unique_ptr<long long[]> ind;
  unique_ptr<long long[]> DOF;
  SpMtx_core(){};
  ~SpMtx_core(){};
};

class AdjMtx_class
{
private:
  unique_ptr<SpMtx_core> Sp_index;
  arma::field<tree_class> tree;

public:
  AdjMtx_class(int);
  ~AdjMtx_class(){};
  void add_clique(const geometry_class &);
  int associate(const geometry_class &, std::unique_ptr<SpMtx_core> &);
};

#endif
