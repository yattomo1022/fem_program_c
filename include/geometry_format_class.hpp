#ifndef geometry_format_class_h
#define geometry_format_class_h

#include <string>
#include <iostream>
#include <map>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

class geometry_format_class
{
private:
  static std::map<std::string, arma::Col<int>::fixed<4>> Phystag_conv;
  geometry_format_class();
  //Eigen::MatrixXd conversion(137, 3);
  //std::unique_ptr<int[]> conversion;

public:
  static void init();
  static int get_Phystag(const std::string tagname);
  static arma::Col<int>::fixed<3> get_elemInfo(const std::string tagname);
};

//extern gmsh2geo_class gmsh2geo;

#endif
