#ifndef tree_class_h
#define tree_class_h

#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <memory>

class tree_class
{
private:
  //public:
  struct tree_node
  {
  public:
    int value;
    std::shared_ptr<tree_node> L; //valueよりも小さいノード
    std::shared_ptr<tree_node> R;
    //public:
    tree_node();
    ~tree_node();
  };

  std::shared_ptr<tree_node> root; //根
  int nVTX;                        //頂点数

public:
  tree_class();
  ~tree_class();
  void insert(const arma::Col<int> &);
  //void get_tree(arma::Col<int>&);
  int get_tree(arma::Col<int> &);
  int get_ntree();
};

#endif
