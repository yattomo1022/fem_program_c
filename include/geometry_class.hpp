#ifndef geometry_class_h
#define geometry_class_h

#include <iostream>
#include <string>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <memory>
using namespace std;

class geometry_class
{
private:
  //構造体定義
  struct tag_type
  {
  public:
    int tag;
    string tagname;
    int ID1;
    int ID2;
    int ID3;
    tag_type();
    ~tag_type(){};
  };

  struct elem_type
  {
  public:
    int sID;
    int nnode;
    int PhysTag;
    // int nDOF;
    arma::Col<int>::fixed<3> elemInfo;
    arma::Col<int>::fixed<3> ID;
    arma::Col<int> itag;
    arma::Col<int> cList; // size = nnode
    elem_type();
    ~elem_type(){};
  };

  struct MPC_type
  {
  public:
    arma::Col<int> DomainType;    // Acoustic, Elastic
    arma::Row<int> Domainnnode;   // domain毎の節点数
    arma::Row<int> DomainelemDOF; // domain毎の節点あたり自由度数
    arma::Mat<int> Table;         //メッシュ節点数*全体ドメイン数
    MPC_type();
    ~MPC_type(){};
  };

  //マクロ的な使い方
  const int BodyType      = 0;
  const int InterfaceType = 1;
  const int DirechletType = 2;
  //const int AC            = 0;

  //クラスメンバ
  string mesh_filename;
  arma::field<tag_type> tagList;
  arma::field<elem_type> elemList;
  arma::Mat<double> nodeList;
  int DOF;
  MPC_type MPC;
  arma::Col<int> DirechletTable;

  //プライベートメソッド（called by mk_MPC）
  int regist_AC(int); // elemNo
  int regist_EM(int) { return 0; };

  int regist_BD_AC(int) { return 0; }; // elemNo, return warning flags
  int regist_IF_AC(int);

  //プライベートメソッド（called by parse_direchlet）
  void regist_AC_direchlet(int i);

  //メソッド
public:
  geometry_class(string s);
  ~geometry_class(){};
  void read_gmsh();
  void mk_MPC();
  void parse_direchlet();
  void write_geoinfo();

  //以降アクセサ

  // 全体要素数取得
  int get_nelem() const;

  // 全体節点数を取得
  int get_nnode() const;

  // 全体総自由度
  int get_DOF() const;

  // その要素番号(0 based)の境界情報
  int get_InterfaceInfo(int) const;

  // その要素番号(0 based)のconnectivityを全体自由度番号(0 based)行列として取得
  void get_MPC_cList(int, arma::Col<int> &, arma::Col<int> &) const;

  // その要素番号(0 based)のconnectivityを節点番号(0 based)行列として取得
  void get_cList(int, arma::Col<int> &) const;

  // その要素番号(0 based)の節点あたり自由度数取得
  int get_elemDOF(int) const;

  // その要素番号(0 based)の要素の節点座標取得
  void get_elemnodeList(arma::mat &, int) const;

  // その要素番号(0 based)の要素の物理IDと形状関数IDを取得
  tuple<int, int> get_sID_PhysTag(int) const;

  // その要素番号(0 based)の要素のdomain番号(1 based)を取得
  int get_elemMPC_Col(int) const;

  // domain毎の場の種類行列を取得
  arma::Col<int> get_DomainType() const;

  // domain番号(1 based)のnodeID, elemID, G2LTagle(全て0 based)を取得
  void get_reconst(arma::Col<int> &, arma::Col<int> &, arma::Col<int> &, int) const;

  // その節点番号(0 based)の節点座標を取得。
  arma::Col<double> get_node(int i) const;

  // その節点番号(0 based)とdomain番号(1 based)についてMPC.Table(0 based)を参照。
  int get_MPCgid(int, int) const;
};
#endif
