#ifndef gmsh_format_class_h
#define gmsh_format_class_h

#include <string>
#include <iostream>
#include <map>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

class gmsh_format_class
{
private:
    static arma::Mat<int> Table;
    gmsh_format_class(); //実装は書かない

public:
    static void init();
    static int get_nv(const int sID);
    static int get_nd(const int sID);
};

//extern gmsh2geo_class gmsh2geo;

#endif