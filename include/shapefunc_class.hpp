#ifndef shapefunc_class_h
#define shapefunc_class_h

#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

class shapefunc_class_Core
{
public: //protected:
  arma::Mat<double> N;
  arma::Cube<double> dNdr;
  arma::Cube<double> dNdr2; //2階微分
  arma::Col<double> wip;    //重み
  arma::Mat<double> Ninv;   //逆行列
  int nnode;                //接点数
  int nip;                  //積分点数
  int sID;                  //sID
  int dim;                  //要素次元
  int dim2;                 //要素次元（2階微分組み合わせ）
public:
  shapefunc_class_Core();
  shapefunc_class_Core(int, int, int, int); //節点，積分点，sID, 次元
  virtual ~shapefunc_class_Core();
  void gauleg(int, arma::Col<double> &, arma::Col<double> &); //積分点数，積分点座標，重み
  virtual void set_SF(){};
  virtual void get_SF(arma::Col<double> &, arma::Mat<double> &,
                      arma::Mat<double> &, int sID, double, double, double){};
};

class SF_L_O1 : public shapefunc_class_Core
{
public:
  //SF_L_O1(){};
  ~SF_L_O1(){};
  SF_L_O1(int, int, int, int);
  void set_SF();
  void get_SF(arma::Col<double> &, arma::Mat<double> &,
              arma::Mat<double> &, int sID, double, double, double);
};

class SF_Tr_O1 : public shapefunc_class_Core
{
public:
  //SF_Tr_O1(){};
  ~SF_Tr_O1(){};
  SF_Tr_O1(int, int, int, int);
  void set_SF();
  void get_SF(arma::Col<double> &, arma::Mat<double> &,
              arma::Mat<double> &, int sID, double, double, double);
};

class SF_Qu_O1 : public shapefunc_class_Core
{
public:
  SF_Qu_O1(){};
  ~SF_Qu_O1(){};
  SF_Qu_O1(int, int, int, int);
  void set_SF();
  void get_SF(arma::Col<double> &, arma::Mat<double> &,
              arma::Mat<double> &, int sID, double, double, double);
};

class SF_Tet_O1 : public shapefunc_class_Core
{
public:
  //SF_Tet_O1(){};
  ~SF_Tet_O1(){};
  SF_Tet_O1(int, int, int, int);
  void set_SF();
  void get_SF(arma::Col<double> &, arma::Mat<double> &,
              arma::Mat<double> &, int sID, double, double, double);
};

#endif
