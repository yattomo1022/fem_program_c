#ifndef interpolate_class_h
#define interpolate_class_h

#include "geometry_class.hpp"
#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

class collision_detection_class
{
private:
    collision_detection_class();

public:
    ~collision_detection_class(){};
    static void find(const geometry_class &, const arma::Mat<double> &);
};

#endif