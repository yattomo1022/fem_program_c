#ifndef FEMPost_Core_h
#define FEMPost_Core_h

#include "geometry_class.hpp"
#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <memory>
#define complex std::complex<double>

class FEMPost_Core
{
protected:
    arma::Col<int> nodeID;
    arma::Col<int> elemID;
    arma::Col<int> G2LTable;
    int domainID;

public:
    FEMPost_Core(int i) { domainID = i; };
    virtual ~FEMPost_Core(){};
    void init(const geometry_class &);
    virtual void reconst_sol(const geometry_class &, const std::unique_ptr<complex[]> &) = 0;
    virtual void VTK_output(const geometry_class &, int) = 0;
};

class FEMPost_dummy_class : public FEMPost_Core
{
public:
    FEMPost_dummy_class(int);
    ~FEMPost_dummy_class(){};
    void reconst_sol(const geometry_class &, const std::unique_ptr<complex[]> &){};
    void VTK_output(const geometry_class &, int){};
};

#endif