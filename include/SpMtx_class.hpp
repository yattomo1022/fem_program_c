#ifndef SpMtx_class_h
#define SpMtx_class_h

#include "AdjMtx_class.hpp"
#include <memory>
#pragma warning push
#pragma warning disable 159
#include <mkl.h>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#pragma warning pop
#define complex std::complex<double>

class SpMtx_class
{
private:
    std::unique_ptr<SpMtx_core> Sp_index;
    std::unique_ptr<complex[]> A;
    std::unique_ptr<complex[]> ext;
    _MKL_DSS_HANDLE_t pt[64];
    MKL_INT iparm[64];
    int nnz;
    int DOF;
    int state;
    /*
    state 0: 疎行列はできてない
    state 1: 疎行列データ構造の初回solve
    state 2: 疎行列データ構造の2回目以降
    state 3: この疎行列を同じ構造・値で解いたことがある
    */

public:
    SpMtx_class();
    ~SpMtx_class(){};
    void debug();
    void init(const geometry_class &, AdjMtx_class &);
    void add_clique_coeff(const arma::Mat<complex> &, const geometry_class &, int); //int is elemNo.
    void add_clique_ext(const arma::Col<complex> &, const geometry_class &, int);   //int is elemNo.
    void solve(std::unique_ptr<complex[]> &, int);
};

#endif