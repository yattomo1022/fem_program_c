#ifndef time_class_h
#define time_class_h

#include<time.h>
#include<string>


class time_class{
private:
  struct tm tm;
  std::string dayofweek[7] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

public:
  time_class();
  ~time_class(){};
  void get_time();

};

#endif
