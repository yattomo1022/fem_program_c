#ifndef FEM_acoustic_class_h
#define FEM_acoustic_class_h

#include "shapefunc_class.hpp"
#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <memory>
#define complex std::complex<double>
//#include"gmsh2geo_class.hpp"

class FEM_acoustic_class
{
private:
    double calc_detJ1(arma::mat &);
    double calc_detJ2(arma::mat &);

public:
    FEM_acoustic_class(){};
    ~FEM_acoustic_class(){};
    void AcoustKM_Mtx(arma::cx_mat &, arma::cx_mat &,
                      const std::unique_ptr<shapefunc_class_Core> &,
                      const arma::Mat<double> &, int); //int is sID
    void AcoustC_Mtx(arma::cx_mat &, const std::unique_ptr<shapefunc_class_Core> &,
                     const arma::Mat<double> &, int); //int is sID
};

#endif