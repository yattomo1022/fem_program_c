#ifndef gmsh2vtk_class_h
#define gmsh2vtk_class_h

#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

class gmsh2vtk_class {
 private:
  struct node_g2v {
   public:
    int vtkID;
    arma::Col<int> cList;
  };
  gmsh2vtk_class();  //実装は書かない。
  static arma::field<node_g2v> table;

 public:
  static void init();
  static int get_vtkcList(int, int);  // sID, 0based array
  static int get_vtkID(int);          // sID
};

#endif