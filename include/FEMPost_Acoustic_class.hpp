#ifndef FEMPost_Acoustic_class_h
#define FEMPost_Acoustic_class_h

#include "geometry_class.hpp"
#include "FEMPost_Core.hpp"
#include <string>
#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <memory>
#define complex std::complex<double>

class FEMPost_Acoustic_class : public FEMPost_Core
{
private:
    static const int max_vtk_output;
    static const arma::field<std::string> dataname;
    static const arma::Col<int> datatype; //vtkOutput_class 参照
    arma::Col<int> vtk_flag;
    arma::Col<complex> pressure;
    arma::Col<double> SPL;
    void calc_SPL();

public:
    FEMPost_Acoustic_class(int);
    ~FEMPost_Acoustic_class(){};
    void reconst_sol(const geometry_class &, const std::unique_ptr<complex[]> &);
    void VTK_output(const geometry_class &, int);
    void init_intpl(const geometry_class &, const arma::Mat<double> &, arma::Col<int> &);
};

#endif