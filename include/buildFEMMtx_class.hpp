#ifndef buildFEMMtx_class_h
#define buildFEMMtx_class_h

#include "shapefunc_class.hpp"
#include "geometry_class.hpp"
#include "SpMtx_class.hpp"
#include "parameter_class.hpp"
#include <iostream>
#include <memory>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

class buildFEMMtx_class //静的メンバ関数
{
private:
    buildFEMMtx_class(){};
    ~buildFEMMtx_class(){};

public:
    static void BuildFEM_Mtx(const geometry_class &,
                             const arma::field<unique_ptr<shapefunc_class_Core>> &,
                             SpMtx_class &, const parameter_class &, double); //double is freq
};

#endif