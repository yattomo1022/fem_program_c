CC      = icpc
#CFLAGS  = -debug all -traceback -Wall -std=c++11 -DARMA_DONT_USE_WRAPPER -MMD -MP
LDFLAGS = 
#LIBS    =  ${MKLROOT}/lib/libmkl_intel_ilp64.a ${MKLROOT}/lib/libmkl_intel_thread.a ${MKLROOT}/lib/libmkl_core.a -liomp5 -lpthread -lm -ldl
INCLUDE = -I./include -I/usr/local/include/armadillo
SRC_DIR = ./src
OBJ_DIR = ./obj
SOURCES = $(shell ls $(SRC_DIR)/*.cpp) 
OBJS    = $(subst $(SRC_DIR),$(OBJ_DIR), $(SOURCES:.cpp=.o))
TARGET  = FEMC
DEPENDS = $(OBJS:.o=.d)

buildtype := debug
ifeq ($(buildtype), release)
	@echo build type is RELEASE
	CFLAGS = -DMKL_ILP64 -O3 -std=c++11 -DARMA_DONT_USE_WRAPPER -MMD -MP
	LIBS    =  ${MKLROOT}/lib/libmkl_intel_ilp64.a ${MKLROOT}/lib/libmkl_intel_thread.a ${MKLROOT}/lib/libmkl_core.a -liomp5 -lpthread -lm -ldl
else ifeq ($(buildtype), debug)
	#@echo "build type is DEBUG"
	CFLAGS = -DMKL_ILP64 -O0 -g -debug all -traceback -Wall -std=c++11 -DARMA_DONT_USE_WRAPPER -MMD -MP
	LIBS = ${MKLROOT}/lib/libmkl_intel_ilp64.a ${MKLROOT}/lib/libmkl_sequential.a ${MKLROOT}/lib/libmkl_core.a -lpthread -lm -ldl
else
	$(error buildtype must be release or debug)
endif

all: $(TARGET)

$(TARGET): $(OBJS)
	@echo Compiling $@
	@$(CC) -o $@ $(OBJS) $(LDFLAGS) $(LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp 
	@if [ ! -d $(OBJ_DIR) ]; \
	then echo "mkdir -p $(OBJ_DIR)"; mkdir -p $(OBJ_DIR); \
	fi
	@echo Compiling $@
	@$(CC) $(CFLAGS) $(INCLUDE) -o $@ -c $<

run:
	./$(TARGET) | tee adjacent_hystory.txt

clean:
	-rm $(OBJS) $(TARGET) $(DEPENDS)

-include $(DEPENDS)
