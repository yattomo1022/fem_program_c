# FEM_PROGRAM C/C++
## geometryの入力方法
### Acoustics
#### Acoustic body
| 番号 | 要素 | 説明
|:--:|:--|:--|
| 0 | acoustic | 音場 |
| 1 | porous   |等価流体モデルの多孔質材（未実装）|
| 2 | pml      | 音場のPML|
| 3 | pml_ibound | PMLの設定（単にiboundだけでも可能）|
| 4 |pml_obound  |PMLの設定（単にoboundだけでも可能）|

#### Acoustic boundary
| 番号 | 要素 | 説明
|:--:|:--|:--|
| 10| admittance | アドミタンス境界|
| 11| vibratory  | 振動境界 |
| 12| force      | 点音源 |
| 13| direchlet  |ディレクレ境界条件（for debug）|

#### Acoustic Interface
| 番号 | 要素 | 説明
|:--:|:--|:--|
|100 | if_ac_ac |  音響要素同士の連成 |

------------------
------------------

## PhysTag Information

各Typeの-1はどれでもないことを示す。
#### BodyType
0. 音響要素群

#### InterfaceType
0. 音響要素の境界群
1. 音響-音響境界

#### DirechletType
0. 音響のディレクレ境界条件

------------------
------------------

## version履歴
* ***release-1.0*** (2020/02/13) 
    * gmshファイルの読み取り実装
    * 音響要素の有限要素法の実装
        * 音響要素，アドミタンス境界，点音源
    * VTK書き出しの実装
        * SPLの書き出し