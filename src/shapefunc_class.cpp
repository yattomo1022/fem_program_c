#include "shapefunc_class.hpp"
#include "main.hpp"
#include <iostream>
#include <cmath>

//コンストラクタ
shapefunc_class_Core::shapefunc_class_Core(int Nv, int Ni, int s, int nd)
{ //節点，積分点，sID, 次元
  nnode = Nv;
  nip = Ni;
  sID = s;
  dim = nd;
  dim2 = nd * (nd + 1) / 2;
  N = arma::Mat<double>(nnode, nip, arma::fill::zeros);            //積分点*節点
  dNdr = arma::Cube<double>(dim, nnode, nip, arma::fill::zeros);   //積分点*xi * 節点
  dNdr2 = arma::Cube<double>(dim2, nnode, nip, arma::fill::zeros); //積分点*xi * 節点
  wip = arma::Col<double>(nip, arma::fill::zeros);
  Ninv = arma::Mat<double>(nip, nnode, arma::fill::zeros);
  //std::cout << "shape_func_class_Core is initialized.\n";
}

shapefunc_class_Core::~shapefunc_class_Core() {}

void shapefunc_class_Core::gauleg(int x, arma::Col<double> &xi, arma::Col<double> &wip)
{
  const double EPSG = pow(10.0, -12.0);
  int m, n;
  double p1, p2, p3, pp, z, z1;
  int k;

  m = (int)(x + 1) / 2;
  n = x;
  //std::cout << m << n << "\n";
  //Newton法で求める。
  for (int i = 1; i < m + 1; ++i)
  {
    z = cos(::PI * ((double)i - 0.25) / ((double)n + 0.5));
    //First time
    p1 = 1.0;
    p2 = 0.0;

    for (int j = 1; j < n + 1; j++)
    {
      p3 = p2;
      p2 = p1;
      p1 = ((2.0 * (double)j - 1.0) * z * p2 - ((double)j - 1.0) * p3) / (double)j;
    }

    pp = (double)n * (z * p1 - p2) / (z * z - 1.0);
    z1 = z;
    z = z1 - p1 / pp;

    //write(*,*) z

    //Refine the accuracy of the solution
    k = 0;
    while (abs(z - z1) > EPSG)
    {
      p1 = 1.0;
      p2 = 0.0;

      for (int j = 1; j < n + 1; j++)
      {
        p3 = p2;
        p2 = p1;
        p1 = ((2.0 * (double)j - 1.0) * z * p2 - ((double)j - 1.0) * p3) / (double)j;
      }

      pp = (double)n * (z * p1 - p2) / (z * z - 1.0);
      z1 = z;
      z = z1 - p1 / pp;

      //for debug
      //if(k > 98) write(*,*) z

      //ループ脱出用
      k = ++k;
      if (k > 100)
      {
        std::cout << "o Shape Func. setting..." << '\n';
        std::cout << " \033[32m<remark>\033[0mgauleg: ニュートン法は収束しなかった可能性あり\n";
        //z = (z + z1)/2.D0
      }
    }
    //std::cout << "asignment start\n";
    //Integral points and their weights
    xi(i - 1) = -z;
    xi(n - i) = z;
    wip(i - 1) = 2.0 / ((1.0 - z * z) * pp * pp);
    wip(n - i) = wip(i - 1);
  }

  //std::cout << wip << "\n";
}

//継承クラス
//---------------------------
// SF_L_O1
//---------------------------
SF_L_O1::SF_L_O1(int a, int b, int c, int d) : shapefunc_class_Core(a, b, c, d) {}

void SF_L_O1::set_SF()
{
  //std::cout << "setting...\n";
  arma::Col<double> x(nip, arma::fill::zeros);
  arma::Col<double> N_t(nnode, arma::fill::zeros);
  arma::Mat<double> dNdr_t(dim, nnode, arma::fill::zeros);
  arma::Mat<double> dNdr2_t(dim2, nnode, arma::fill::zeros);

  //std::cout << "gauleg start...\n";
  shapefunc_class_Core::gauleg(nip, x, wip);
  //std::cout << "gauleg end...\n";

  for (int i = 0; i < nip; ++i)
  {
    SF_L_O1::get_SF(N_t, dNdr_t, dNdr2_t, sID, x(i), 0.0, 0.0);

    N.col(i) = N_t;
    dNdr.slice(i) = dNdr_t;
    dNdr2.slice(i) = dNdr2_t;
  }
}

void SF_L_O1::get_SF(arma::Col<double> &N_t, arma::Mat<double> &dNdr_t, arma::Mat<double> &dNdr2_t,
                     int s, double x, double y, double z)
{
  if (s != 1)
  {
    std::cout << "o Shape Func. setting..." << '\n';
    std::cout << " \a\x1b[31mERROR: \x1b[0mFail to get the Shape function(SF_L_O1)";
    exit(1); //for check

    N_t(0) = 0.5 * (1.0 - x);
    N_t(1) = 0.5 * (1 + x);
    dNdr_t(0, 0) = -0.5;
    dNdr_t(0, 1) = 0.5;
    dNdr2_t(0, 0) = 0.0;
    dNdr2_t(0, 1) = 0.0;
  }
}

//---------------------------
// SF_Tr_O1
//---------------------------
SF_Tr_O1::SF_Tr_O1(int a, int b, int c, int d) : shapefunc_class_Core(a, b, c, d) {}

void SF_Tr_O1::set_SF()
{
  arma::Col<double> xi(nip, arma::fill::zeros);
  arma::Col<double> eta(nip, arma::fill::zeros);
  arma::Col<double> N_t(nnode, arma::fill::zeros);
  arma::Mat<double> dNdr_t(dim, nnode, arma::fill::zeros);
  arma::Mat<double> dNdr2_t(dim2, nnode, arma::fill::zeros);
  //積分点
  xi(0) = 1.0 / 3.0;
  xi(1) = 3.0 / 5.0;
  xi(2) = 1.0 / 5.0;
  xi(3) = 1.0 / 5.0;
  eta(0) = 1.0 / 3.0;
  eta(1) = 1.0 / 5.0;
  eta(2) = 3.0 / 5.0;
  eta(3) = 1.0 / 5.0;
  //重み
  wip(0) = -9.0 / 16.0 / 2.0;
  wip(1) = 25.0 / 48.0 / 2.0;
  wip(2) = 25.0 / 48.0 / 2.0;
  wip(3) = 25.0 / 48.0 / 2.0;

  for (int i = 0; i < nip; ++i)
  {
    SF_Tr_O1::get_SF(N_t, dNdr_t, dNdr2_t, sID, xi(i), eta(i), 0.0);

    N.col(i) = N_t;
    dNdr.slice(i) = dNdr_t;
    dNdr2.slice(i) = dNdr2_t;
  }
}

void SF_Tr_O1::get_SF(arma::Col<double> &N_t, arma::Mat<double> &dNdr_t, arma::Mat<double> &dNdr2_t, int s,
                      double x, double y, double z)
{
  if (s != 2)
  {
    std::cout << "o Shape Func. setting..." << '\n';
    std::cout << " \a\x1b[31mERROR: \x1b[0mFail to get the Shape function(SF_Tr_O1)";
    exit(1); //for check

    //Shape function
    N_t(0) = 1.0 - x - y;
    N_t(1) = x;
    N_t(2) = y;

    //Derivative of Shape functions in local coordinate
    dNdr_t(0, 0) = -1.0;
    dNdr_t(1, 0) = -1.0;
    dNdr_t(0, 1) = 1.0;
    dNdr_t(1, 1) = 0.0;
    dNdr_t(0, 2) = 0.0;
    dNdr_t(1, 2) = 1.0;

    dNdr2_t.fill(0.0);
  }
}

//---------------------------
// SF_Qu_O1
//---------------------------
SF_Qu_O1::SF_Qu_O1(int a, int b, int c, int d) : shapefunc_class_Core(a, b, c, d) {}

void SF_Qu_O1::set_SF()
{
  arma::Col<double> xi(nip / 2, arma::fill::zeros);
  arma::Col<double> eta(nip / 2, arma::fill::zeros);
  arma::Col<double> w(nip / 2, arma::fill::zeros);
  arma::Col<double> N_t(nnode, arma::fill::zeros);
  arma::Mat<double> dNdr_t(dim, nnode, arma::fill::zeros);
  arma::Mat<double> dNdr2_t(dim2, nnode, arma::fill::zeros);
  int id = 0;

  //std::cout << nip/2 << '\n';
  shapefunc_class_Core::gauleg(nip / 2, xi, w);
  eta = xi;

  for (int i = 0; i < nip / 2; ++i)
  {
    for (int j = 0; j < nip / 2; ++j)
    {

      //std::cout << dim2 << '\n';
      SF_Qu_O1::get_SF(N_t, dNdr_t, dNdr2_t, sID, xi(i), eta(j), 0.0);

      wip(id) = w(i) * w(j);     //std::cout << "1\n";
      N.col(id) = N_t;           //std::cout << "2\n";
      dNdr.slice(id) = dNdr_t;   //std::cout << "3\n";
      dNdr2.slice(id) = dNdr2_t; //std::cout << "4\n";
      id++;
    }
  }
}

void SF_Qu_O1::get_SF(arma::Col<double> &N_t, arma::Mat<double> &dNdr_t, arma::Mat<double> &dNdr2_t, int s,
                      double x, double y, double z)
{
  if (s != 3)
  {
    std::cout << "o Shape Func. setting..." << '\n';
    std::cout << " \a\x1b[31mERROR: \x1b[0mFail to get the Shape function(SF_Qu_O1)";
    exit(1);
  } //for check

  //Shape function
  N_t(0) = 0.250 * (1.0 - x) * (1.0 - y);
  N_t(1) = 0.250 * (1.0 + x) * (1.0 - y);
  N_t(2) = 0.250 * (1.0 + x) * (1.0 + y);
  N_t(3) = 0.250 * (1.0 - x) * (1.0 + y);

  //Derivative of Shape functions in local coordinate
  dNdr_t(0, 0) = -0.250 * (1.0 - y);
  dNdr_t(1, 0) = -0.250 * (1.0 - x);
  dNdr_t(0, 1) = 0.250 * (1.0 - y);
  dNdr_t(1, 1) = -0.250 * (1.0 + x);
  dNdr_t(0, 2) = 0.250 * (1.0 + y);
  dNdr_t(1, 2) = 0.250 * (1.0 + x);
  dNdr_t(0, 3) = -0.250 * (1.0 + y);
  dNdr_t(1, 3) = 0.250 * (1.0 - x);

  //del2 N / del xi2 = 0;
  dNdr2_t.fill(0.0);
  //2 * del2 N / del xi eta
  dNdr2_t(2, 0) = 2.0 * (0.250);
  dNdr2_t(2, 1) = 2.0 * (-0.250);
  dNdr2_t(2, 2) = 2.0 * (0.250);
  dNdr2_t(2, 3) = 2.0 * (-0.250);
}

//---------------------------
// SF_Tet_O1
//---------------------------
SF_Tet_O1::SF_Tet_O1(int a, int b, int c, int d) : shapefunc_class_Core(a, b, c, d) {}

void SF_Tet_O1::set_SF()
{
  arma::Col<double> xi(nip, arma::fill::zeros);
  arma::Col<double> eta(nip, arma::fill::zeros);
  arma::Col<double> zeta(nip, arma::fill::zeros);
  arma::Col<double> N_t(nnode, arma::fill::zeros);
  arma::Mat<double> dNdr_t(dim, nnode, arma::fill::zeros);
  arma::Mat<double> dNdr2_t(dim2, nnode, arma::fill::zeros);
  //積分点
  xi(0) = 0.138196601125015;
  xi(1) = 0.5854101966249685;
  xi(2) = 0.138196601125015;
  xi(3) = 0.138196601125015;

  eta(0) = 0.138196601125015;
  eta(1) = 0.138196601125015;
  eta(2) = 0.5854101966249685;
  eta(3) = 0.138196601125015;

  zeta(0) = 0.138196601125015;
  zeta(1) = 0.138196601125015;
  zeta(2) = 0.138196601125015;
  zeta(3) = 0.5854101966249685;

  //重み
  wip.fill(0.25 * (1.0 / 6.0));

  for (int i = 0; i < nip; ++i)
  {
    SF_Tet_O1::get_SF(N_t, dNdr_t, dNdr2_t, sID, xi(i), eta(i), zeta(i));

    N.col(i) = N_t;
    dNdr.slice(i) = dNdr_t;
    dNdr2.slice(i) = dNdr2_t;
  }
}

void SF_Tet_O1::get_SF(arma::Col<double> &N_t, arma::Mat<double> &dNdr_t, arma::Mat<double> &dNdr2_t, int s,
                       double x, double y, double z)
{
  if (s != 4)
  {
    std::cout << "o Shape Func. setting..." << '\n';
    std::cout << " \a\x1b[31mERROR: \x1b[0mFail to get the Shape function(SF_Tet_O1)";
    exit(1);
  } //for check

  //Shape function
  N_t(0) = 1.0 - x - y - z;
  N_t(1) = x;
  N_t(2) = y;
  N_t(3) = z;

  //Derivative of Shape functions in local coordinate
  dNdr_t(0, 0) = -1.0;
  dNdr_t(0, 1) = 1.0;
  dNdr_t(0, 2) = 0.0;
  dNdr_t(0, 3) = 0.0;

  dNdr_t(1, 0) = -1.0;
  dNdr_t(1, 1) = 0.0;
  dNdr_t(1, 2) = 1.0;
  dNdr_t(1, 3) = 0.0;

  dNdr_t(2, 0) = -1.0;
  dNdr_t(2, 1) = 0.0;
  dNdr_t(2, 2) = 0.0;
  dNdr_t(2, 3) = 1.0;

  //del2 N / del xi2 = 0;
  dNdr2_t.fill(0.0);
}