




  /*
  // C++のiostreamは非常に遅いらしいので，
  // C likeな書き方をする。
  
  FILE*  fp;

  fp = fopen(mesh_filename.c_str(), "r");
  
  if(fp == nullptr){
    std::cout << "\a\x1b[31mERROR: \x1b[0mFailed to open file." << '\n';
    return -1;
  }else {
    std::cout << "Reading " << mesh_filename << "..." << '\n';
  }

  //4行読み飛ばし
  char buffer[256];
  for(int i=0; i<4; ++i){
    fgets(buffer, 256, fp); //  printf("%s", buffer);
    if(buffer[strlen(buffer)-1] != '\n'){
      std::cout << "\a\x1b[31mERROR: \x1b[0mBuffer overflow." << '\n';
      return -1;
    }
  }


  //tagname read
  int num;
  fscanf(fp, "%d\n", &num); //  std::cout << num << '\n';
  tagList = new tag_type[num];

  int idum1, idum2;
  char tmp[64];
  char cdum[64];

  for(int i=0; i<num; ++i){
    fgets(buffer, 256, fp);
    if(buffer[strlen(buffer)-1] != '\n'){
      std::cout << "\a\x1b[31mERROR: \x1b[0mBuffer overflow." << '\n';
      return -1;
    }

    sscanf(buffer, "%d %d \" %s  %d %d %d", &idum1, &idum2, tmp,
	   &tagList[i].ID1, &tagList[i].ID2, &tagList[i].ID3 );

     tagList[i].tagname =  tmp;

     //std::cout << buffer<< '\n';
    //std::cout << idum1 << idum2 <<  tagList[i].tagname << tagList[i].ID1 << tagList[i].ID2 << tagList[i].ID3 << '\n';

  }

  //2行読み飛ばし
  for(int i=0; i<2; ++i){
    fgets(buffer, 256, fp); //  printf("%s", buffer);
    if(buffer[strlen(buffer)-1] != '\n'){
      std::cout << "\a\x1b[31mERROR: \x1b[0mBuffer overflow." << '\n';
      return -1;
    }
  }

  //Node List
  fscanf(fp, "%d\n", &num); //  std::cout << num << '\n';

  nodeList = Eigen::MatrixXd(num,3);
  
  //std::cout << nodeList.rows() << " "<<nodeList.cols() << '\n';
  double tmp3d[3];
  
  for(int i=0; i<num; ++i){
    fgets(buffer, 256, fp);
    if(buffer[strlen(buffer)-1] != '\n'){
      std::cout << "\a\x1b[31mERROR: \x1b[0mBuffer overflow." << '\n';
      return -1;
    }

    sscanf(buffer, "%d %3lf", &idum1, &tmp3d);
    for(int j=0; j<3; j++){
      nodeList(i,j) = tmp3d[j];
    }


    
  }

  
  
  
  fclose(fp);
  return 0;
  */
