#include "SpMtx_class.hpp"
#include "AdjMtx_class.hpp"
#include "utility_tool_class.hpp"
#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#define complex std::complex<double>

SpMtx_class::SpMtx_class()
{
    Sp_index = nullptr;
    state = 0;
}

void SpMtx_class::debug()
{
    for (int i = 0; i < nnz; ++i)
    {
        std::cout << A[i] << "\n";
    }
}

void SpMtx_class::init(const geometry_class &geo, AdjMtx_class &AdjMtx)
{

    if (state == 0)
    {
        std::cout << "  +---- Initializing sparse matrix from the adjacent list..." << '\n';
        nnz = AdjMtx.associate(geo, Sp_index);
        DOF = geo.get_DOF();

        if (Sp_index == nullptr)
        {
            std::cout << " \a\x1b[31mERROR: \x1b[0mError in SpMtx initialization.\n";
            exit(1);
        }

        A.reset(new complex[nnz]);
        ext.reset(new complex[DOF]);
        state = 1;
        std::cout << "\e[1F  +---- [\a\x1b[32mEnd\x1b[0m]Initializing sparse matrix from the adjacent list." << '\n';
    }
    else if (state == 1)
    {
        //nothing to do
    }
    else
    {
        std::cout << "        *Initializing sparse matrix to zero." << '\n';
        for (int i = 0; i < nnz; ++i)
        {
            A[i] = complex(0.0, 0.0);
        }
        for (int i = 0; i < DOF; ++i)
        {
            ext[i] = complex(0.0, 0.0);
        }
        state = 2;
    }
}

void SpMtx_class::add_clique_coeff(const arma::Mat<complex> &Mat, const geometry_class &geo, int i)
{
    arma::Col<int> buf;
    arma::Col<int> buf_di;
    int nDOF = geo.get_elemDOF(i);
    geo.get_MPC_cList(i, buf, buf_di);

    int sumDOF = buf.n_rows;
    int sumDOF_di = buf_di.n_rows;
    int loc;
    bool exist;
    int p;

    if (sumDOF + sumDOF_di != Mat.n_rows || sumDOF + sumDOF_di != Mat.n_cols)
    {
        std::cout << " \a\x1b[31mERROR: \x1b[0mError in SpMtx adding clique.\n";
        exit(1);
    }
    else
    {
        //ディレクレ境界条件以外
        for (int i = 0; i < sumDOF; ++i)
        {
            for (int j = 0; j < sumDOF; ++j)
            {
                std::tie(loc, exist) = utility_tool_class::binary_search(buf(i), &(Sp_index->DOF[Sp_index->ind[buf(j)]]), &(Sp_index->DOF[Sp_index->ind[buf(j) + 1] - 1]));
                if (!exist)
                {
                    std::cout << " \a\x1b[31mERROR: \x1b[0mError in SpMtx adding clique.\n";
                    exit(1);
                }
                p = Sp_index->ind[buf(j)] + loc;
                A[p] += Mat(i, j);
            }
        }
        //ディレクレ境界条件
        for (int i = 0; i < sumDOF_di; ++i)
        {
            std::tie(loc, exist) = utility_tool_class::binary_search(buf_di(i), &(Sp_index->DOF[Sp_index->ind[buf_di(i)]]), &(Sp_index->DOF[Sp_index->ind[buf_di(i) + 1] - 1]));
            if (!exist)
            {
                std::cout << " \a\x1b[31mERROR: \x1b[0mError in SpMtx adding clique.\n";
                exit(1);
            }
            p = Sp_index->ind[buf_di(i)] + loc;
            A[p] = complex(1.0, 0.0);
        }
    }
}

void SpMtx_class::add_clique_ext(const arma::Col<complex> &ext_t, const geometry_class &geo, int i)
{
    arma::Col<int> buf;
    arma::Col<int> buf_di;
    int nDOF = geo.get_elemDOF(i);
    geo.get_MPC_cList(i, buf, buf_di);

    int sumDOF = buf.n_rows;
    int sumDOF_di = buf_di.n_rows;
    int loc;
    bool exist;
    int p;

    if (sumDOF + sumDOF_di != ext_t.n_rows)
    {
        std::cout << " \a\x1b[31mERROR: \x1b[0mError in ext adding clique.\n";
        exit(1);
    }
    else
    {
        //ディレクレ境界条件以外
        for (int i = 0; i < sumDOF; ++i)
        {
            ext[buf(i)] = ext_t(i);
        }
        //ディレクレ境界条件
        if (sumDOF_di != 0)
        {
            std::cout << " \a\x1b[31mERROR: \x1b[0mError in ext adding clique.\n";
        }
    }
}

void SpMtx_class::solve(std::unique_ptr<complex[]> &sol, int flag)
{
    MKL_INT maxfct = 1; //In most applications this value is equal to 1
    MKL_INT mnum = 1;   //In most applications this value is 1
    MKL_INT mtype = 13;
    /*
    1 : real and structurally symmetric
    2 : real and symmetric positive definite
    -2: real and symmetric indefinite
    3 : complex and structurally symmetric
    4 : complex and Hermitian positive definite
    -4: complex and Hermitian indefinite
    6 : complex and symmetric
    11: real and nonsymmetric
    13: complex and nonsymmetric
    */
    MKL_INT phase;            //Analysis, numerical factorization, solve, iterative refinement
    MKL_INT n = (MKL_INT)DOF; //Number of equations
    MKL_INT perm = 0;         //unknown detail
    MKL_INT nrhs = 1;         //Number of right-hand sides
    MKL_INT msglvl = 0;       //Message level information(1: Yes, 0: No)
    MKL_INT error = 0;

    std::cout << "        *Solve by Intel MKL Pardiso\n";
    sol.reset(new complex[DOF]);
    complex ddum;

    //std::cout << "initialized sol: ";

    /*     for (int i = 0; i < DOF; ++i)
    {
        std::cout << sol[i];
    }
    std::cout << "\n"; */

    switch (state)
    {
    case 1:
        phase = 13;
        pardisoinit(pt, &mtype, iparm);
        iparm[26] = 1; //output message from pardiso
        iparm[34] = 1; //Zero based indexing
        break;
    case 2:
        phase = 23;
        break;
    case 3:
        phase = 33;
        break;
    default:
        std::cout << " \a\x1b[31mERROR: \x1b[0mPradiso: invalid phase ->" << state << "\n";
        exit(1);
    }

    //std::cout << sol[0] << "\n";
    //std::cout << &sol[0] << "\n";

    pardiso(pt, &maxfct, &mnum, &mtype, &phase, &n,
            &A[0], &Sp_index->ind[0], &Sp_index->DOF[0],
            &perm, &nrhs, iparm, &msglvl, &ext[0], &sol[0], &error);

    switch (error)
    {
    case 0:
        std::cout << "\e[1F        *[\a\x1b[32mEnd\x1b[0m]Solve by Intel MKL Pardiso\n";
        state = 3;
        break;
    case -1:
        std::cout << " \a\x1b[31mERROR: \x1b[0mPradiso: input inconsistent.\n";
        exit(1);
        break;
    case -2:
        std::cout << " \a\x1b[31mERROR: \x1b[0mPradiso: not enough memory.\n";
        exit(1);
        break;
    case -3:
        std::cout << " \a\x1b[31mERROR: \x1b[0mPradiso: reordering problem.\n";
        exit(1);
        break;
    case -4:
        std::cout << " \a\x1b[31mERROR: \x1b[0mPradiso: not enough memory.\n";
        exit(1);
        break;
    default:
        std::cout << " \a\x1b[31mERROR: \x1b[0mPradiso: Error is occured.\n";
        exit(1);
        break;
    }

    if (flag == 1)
    {
        std::cout << "        *Finilize Intel MKL Pardiso\n";
        phase = -1;
        pardiso(pt, &maxfct, &mnum, &mtype, &phase, &n,
                &ddum, &Sp_index->ind[0], &Sp_index->DOF[0],
                &perm, &nrhs, iparm, &msglvl, &ddum, &ddum, &error);
        std::cout << "\e[1F        *[\a\x1b[32mEnd\x1b[0m]Finilize Intel MKL Pardiso\n";
        state = 0;
    }
}
