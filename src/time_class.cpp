#include<iostream>
#include<time_class.hpp>
#include<time.h>

time_class::time_class(){
  
}

void time_class::get_time(){
  time_t t = time(nullptr);
  localtime_r(&t, &tm);
  printf("%04d/%02d/%02d %s %02d:%02d:%02d\n",
	 tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
	 dayofweek[tm.tm_wday].c_str(), tm.tm_hour, tm.tm_min, tm.tm_sec);

}
