#include "gmsh2vtk_class.hpp"
#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

arma::field<gmsh2vtk_class::node_g2v> gmsh2vtk_class::table;

void gmsh2vtk_class::init()
{
    table = arma::field<node_g2v>(131);
    //1st order line
    table(0).vtkID = 3;
    table(0).cList = {0, 1};
    //1st order triangle
    table(1).vtkID = 5;
    table(1).cList = {0, 1, 2};
    //1st order quadrangle
    table(2).vtkID = 9;
    table(2).cList = {0, 1, 2, 3};

    //point
    table(14).vtkID = 1;
    table(14).cList = {0};
}

int gmsh2vtk_class::get_vtkcList(int i, int j) //sID, 0based array
{
    return table(i - 1).cList(j);
}

int gmsh2vtk_class::get_vtkID(int i)
{
    return table(i - 1).vtkID;
}