#include "vtkOutput_class.hpp"
#include "geometry_class.hpp"
#include "gmsh2vtk_class.hpp"
#include <fstream>
#include <iostream>
#include <string>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

const int vtkOutput_class::_scalar = 0;
const int vtkOutput_class::_vector = 1;

void vtkOutput_class::vtkOutput_header(ofstream &fout, string filename, int nnode, int nelem)
{

  fout.open(filename);
  if (!fout)
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mVTK:file can't be opend. ->" << filename << "\n";
    exit(1);
  }
  fout << "<?xml version=\"1.0\" ?>\n";
  fout << "<VTKFile type=\"UnstructuredGrid\">\n";
  fout << "    <UnstructuredGrid>\n";
  fout << "        <Piece NumberOfPoints=\"" << nnode << "\" NumberOfCells=\"" << nelem << "\">\n";
}

void vtkOutput_class::vtkOutput_pointdata(ofstream &fout, const arma::Col<int> &datatype,
                                          const arma::field<std::string> &dataname, arma::Mat<double> *ptr[])
{
  int b_scalar = -1;
  int b_vector = -1;
  int n_data = dataname.n_elem;

  if (n_data != datatype.n_elem)
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mMismatch among number of datatype, dataname and data\n";
    fout.close();
    exit(1);
  }
  for (int i = 0; i < n_data; ++i)
  {
    if (datatype(i) == _scalar)
    {
      b_scalar = i;
      break;
    }
  }
  for (int i = 0; i < n_data; ++i)
  {
    if (datatype(i) == _vector)
    {
      b_vector = i;
      break;
    }
  }
  fout << "                <PointData " << flush;
  if (b_scalar == -1 && b_vector == -1)
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mNot either scalar and vector is found\n";
    fout.close();
    exit(1);
  }
  if (b_scalar != -1)
  {
    fout << "Scalars=\"" << dataname(b_scalar) << "\" " << flush;
  }
  if (b_vector != -1)
  {
    fout << "Vectors=\"" << dataname(b_vector) << "\" " << flush;
  }
  fout << ">\n";

  //int n_data = dataname.n_elem;
  for (int i = 0; i < n_data; ++i)
  {
    if (ptr[i] != nullptr)
    {
      fout << "                    <DataArray type=\"Float64\" Name=\"" << dataname(i) << "\" format=\"ascii\">\n";
      fout << (*ptr[0]).t();
      fout << "                    </DataArray>\n";
    }
  }

  fout << "                </PointData>\n";
}

/* void vtkOutput_class::vktOutput_pdatafooter(ofstream &fout)
{
} */

void vtkOutput_class::vtkOutput_point(ofstream &fout, const geometry_class &geo, const arma::Col<int> &nodeID)
{
  int nnode = nodeID.n_rows;
  fout << "                <Points>\n";
  fout << "                    <DataArray NumberOfComponents=\"3\" type=\"Float64\" format=\"ascii\">\n";
  for (int i = 0; i < nnode; ++i)
  {
    fout << (geo.get_node(nodeID(i))).t();
  }
  fout << "                    </DataArray>\n";

  fout << "                </Points>\n";
}

void vtkOutput_class::vtkOutput_cell(ofstream &fout, const geometry_class &geo, const arma::Col<int> &elemID, arma::Col<int> &g2lTable)
{
  int n_elem = elemID.n_rows;
  int n_cList;
  arma::Col<int> cList;
  arma::Row<int> offset(n_elem, arma::fill::zeros);
  int idx_offset = 0;
  fout << "                <Cells>\n";
  //connectivity
  int sID;
  fout << "                    <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n";
  for (int i = 0; i < n_elem; ++i)
  {
    std::tie(sID, std::ignore) = geo.get_sID_PhysTag(elemID(i));
    geo.get_cList(elemID(i), cList);
    n_cList = cList.n_rows;
    idx_offset += cList.n_rows;
    offset(i) = idx_offset;
    //std::cout << cList.t();
    for (int j = 0; j < n_cList; ++j)
    {
      //std::cout << g2lTable(cList(gmsh2vtk_class::get_vtkcList(sID - 1, j) - 1) - 1) << " ";
      fout << g2lTable(cList(gmsh2vtk_class::get_vtkcList(sID, j))) << " ";
    }
    //std::cout << "\n";
  }
  fout << "\n";
  fout << "                    </DataArray>\n";
  //offset
  fout << "                    <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n";
  fout << offset;
  fout << "                    </DataArray>\n";
  //types
  fout << "                    <DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n";
  for (int j = 0; j < n_elem; ++j)
  {
    std::tie(sID, std::ignore) = geo.get_sID_PhysTag(elemID(j));
    fout << gmsh2vtk_class::get_vtkID(sID) << " ";
  }
  fout << "\n";
  fout << "                    </DataArray>\n";
  fout << "                </Cells>\n";
}

void vtkOutput_class::vktOutput_footer(ofstream &fout)
{
  fout << "        </Piece>\n";
  fout << "    </UnstructuredGrid>\n";
  fout << "</VTKFile>\n";
  fout.close();
}
