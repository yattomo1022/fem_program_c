#include "FEM_acoustic_class.hpp"
#include "shapefunc_class.hpp"
#include "gmsh_format_class.hpp"
#include <iostream>
#include <memory>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

void FEM_acoustic_class::AcoustKM_Mtx(arma::cx_mat &K, arma::cx_mat &M,
                                      const std::unique_ptr<shapefunc_class_Core> &SF,
                                      const arma::Mat<double> &node, int sID)
{
    int nMat = SF->nnode;
    int elemdim = gmsh_format_class::get_nd(sID);
    int nip = SF->nip;
    double detJ;
    arma::mat J;
    arma::mat Bmat;

    K = arma::cx_mat(nMat, nMat, arma::fill::zeros);
    M = arma::cx_mat(nMat, nMat, arma::fill::zeros);

    for (int i = 0; i < nip; ++i)
    {
        J = SF->dNdr.slice(i) * node.t();
        Bmat = arma::solve(J, SF->dNdr.slice(i));
        switch (elemdim)
        {
        case 1:
            detJ = calc_detJ1(J);
            break;
        case 2:
            detJ = calc_detJ2(J);
            break;
        case 3:
            detJ = arma::det(J);
            break;
        default:
            std::cout << " \a\x1b[31mERROR: \x1b[0mAcoustMat: Invalid convertion from gmsh to geo.\n";
            exit(1);
        }
        M += arma::conv_to<arma::cx_mat>::from(SF->N.col(i) * (SF->N.col(i)).t() * detJ * SF->wip(i));
        K += arma::conv_to<arma::cx_mat>::from(Bmat.t() * Bmat * detJ * SF->wip(i));
    }
}

void FEM_acoustic_class::AcoustC_Mtx(arma::cx_mat &C, const std::unique_ptr<shapefunc_class_Core> &SF,
                                     const arma::Mat<double> &node, int sID)
{
    int nMat = SF->nnode;
    int elemdim = gmsh_format_class::get_nd(sID);
    int nip = SF->nip;
    double detJ;
    arma::mat J;
    arma::mat Bmat;

    C = arma::cx_mat(nMat, nMat, arma::fill::zeros);

    for (int i = 0; i < nip; ++i)
    {
        J = SF->dNdr.slice(i) * node.t();
        Bmat = arma::solve(J, SF->dNdr.slice(i));
        switch (elemdim)
        {
        case 1:
            detJ = calc_detJ1(J);
            break;
        case 2:
            detJ = calc_detJ2(J);
            break;
        case 3:
            std::cout << " \a\x1b[31mERROR: \x1b[0mC Mtx. of Acoustic is invalid in 3D integration.\n";
            exit(1);
            break;
        default:
            std::cout << " \a\x1b[31mERROR: \x1b[0mAcoustMat: Invalid convertion from gmsh to geo.\n";
            exit(1);
        }
        C += arma::conv_to<arma::cx_mat>::from(SF->N.col(i) * SF->N.col(i).t() * detJ * SF->wip(i));
    }
}

double FEM_acoustic_class::calc_detJ1(arma::mat &J)
{
    return std::sqrt(std::pow(J(0, 0), 2) + std::pow(J(0, 1), 2) + std::pow(J(0, 2), 2));
}

double FEM_acoustic_class::calc_detJ2(arma::mat &J)
{
    return std::sqrt(std::pow(J(0, 1) * J(1, 2) - J(0, 2) * J(1, 1), 2.0) + std::pow(J(0, 2) * J(1, 0) - J(0, 0) * J(1, 2), 2.0) + std::pow(J(0, 0) * J(1, 1) - J(0, 1) * J(1, 0), 2.0));
}
