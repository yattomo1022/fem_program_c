#include <iostream>
#include <ctype.h>
#include "utility_tool_class.hpp"

char *utility_tool_class::StrToLower(char *s)
{
  char *p; /* 作業用ポインタ */
  /* for (  )ループの初期化の式で、pは文字列の
     先頭アドレスを指すように初期化される */
  for (p = s; *p; p++)
  {                   /* pがヌルでなければ */
    *p = tolower(*p); /* pの指す中身を大文字に変換 */
  }
  return (s); /* 文字列の先頭アドレスを返す */
}

std::tuple<int, bool> utility_tool_class::binary_search(int j, const long long *st, const long long *end)
{
  int left = 0;
  int middle;
  int right = end - st;
  bool it_exists;

  while (left <= right)
  {
    middle = (left + right) / 2; //どうせ最適化がかかる

    if (j <= *(st + middle))
    {
      right = middle - 1;
    }
    if (*(st + middle) <= j)
    {
      left = middle + 1;
    }
  }

  it_exists = (left - right == 2);
  if (it_exists)
  {
    return std::tie(middle, it_exists);
  }
  else
  {
    return std::tie(left, it_exists);
  }
}
