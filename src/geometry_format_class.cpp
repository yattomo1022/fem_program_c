#include "geometry_format_class.hpp"
#include <iostream>
#include <map>
#include <string>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

std::map<std::string, arma::Col<int>::fixed<4>>
    geometry_format_class::Phystag_conv;

void geometry_format_class::init()
{
  // buildFEMMtxがこれに依存！
  // Acoustics（自由度1）        （PhysTag, bodytype, interfacetype, direchlettype）
  Phystag_conv["acoustic"]   = {0, 0, -1, -1};
  Phystag_conv["porous"]     = {1, 0, -1, -1};
  Phystag_conv["pml"]        = {2, 0, -1, -1};
  Phystag_conv["pml_ibound"] = {3, -1, -1, -1};
  Phystag_conv["ibound"]     = {3, -1, -1, -1};
  Phystag_conv["pml_obound"] = {4, -1, -1, -1};
  Phystag_conv["obound"]     = {4, -1, -1, -1};

  // Acoustic boundaries
  Phystag_conv["admittance"] = {10, -1, 0, -1};
  Phystag_conv["vibratory"]  = {11, -1, 0, -1};
  Phystag_conv["force"]      = {12, -1, 0, -1};
  Phystag_conv["direchlet"]  = {13, -1, -1, 0};

  // Acoustic interface
  Phystag_conv["if_ac_ac"] = {100, -1, 1, -1};
}

int geometry_format_class::get_Phystag(const std::string tagname)
{
  try
  {
    return Phystag_conv.at(tagname)(0);
  }
  catch (std::out_of_range)
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mInvalid Phystag.\n";
  }
  return -1;
}

arma::Col<int>::fixed<3> geometry_format_class::get_elemInfo(const std::string tagname)
{
  try
  {
    return Phystag_conv.at(tagname).rows(1, 3);
  }
  catch (std::out_of_range)
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mInvalid Phystag.\n";
  }
  arma::Col<int>::fixed<3> hoge = {-1, -1, -1};
  return hoge;
}
