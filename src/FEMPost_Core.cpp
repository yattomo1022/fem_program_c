#include "geometry_class.hpp"
#include "FEMPost_Core.hpp"
#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

void FEMPost_Core::init(const geometry_class &geo)
{
    geo.get_reconst(nodeID, elemID, G2LTable, domainID);
}

FEMPost_dummy_class::FEMPost_dummy_class(int a) : FEMPost_Core(a)
{
    std::cout << "  +---- dummy is called because of no continuous domain No." << domainID << "\n";
}