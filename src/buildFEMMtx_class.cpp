#include "buildFEMMtx_class.hpp"
#include "FEM_acoustic_class.hpp"
#include "SpMtx_class.hpp"
#include "geometry_class.hpp"
#include "parameter_class.hpp"
#include "shapefunc_class.hpp"
#include "main.hpp"
#include <iostream>
#include <memory>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#define complex std::complex<double>

void buildFEMMtx_class::BuildFEM_Mtx(const geometry_class &geo,
                                     const arma::field<unique_ptr<shapefunc_class_Core>> &SF,
                                     SpMtx_class &SpMtx,
                                     const parameter_class &param, double freq)
{
    arma::mat nList;
    FEM_acoustic_class AC;
    int nelem = geo.get_nelem();
    int PhysTag = 0;
    int sID = 0;
    int nlines = 0;
    double wavenum;

    arma::cx_mat K;
    arma::cx_mat M;
    arma::cx_mat C;
    arma::cx_vec P;

    std::cout << "        *Build FEM Matrix\n";

    for (int i = 0; i < nelem; ++i)
    {
        geo.get_elemnodeList(nList, i);
        std::tie(sID, PhysTag) = geo.get_sID_PhysTag(i);
        wavenum = 2.0 * ::PI * freq / param.get_air_c0(geo.get_elemMPC_Col(i));

        switch (PhysTag)
        {
        case 0: //acoustic
            AC.AcoustKM_Mtx(K, M, SF(sID - 1), nList, sID);
            SpMtx.add_clique_coeff(K - std::pow(wavenum, 2.0) * M, geo, i);
            break;
        case 10: //acoustic admittance
            AC.AcoustC_Mtx(C, SF(sID - 1), nList, sID);
            SpMtx.add_clique_coeff(::ci * wavenum * C, geo, i);
            break;
        case 12: //acoustic point force
            P = arma::cx_vec(1);
            P = complex(1.0, 0.0);
            SpMtx.add_clique_ext(P, geo, i);
            break;
        case 100:
            //nothing to do
            break;
        default:
            std::cout << " \a\x1b[35mWarning: \x1b[0mBuildFEM_Mtx: not yet compatible. ID->" << PhysTag << "\n";
            ++nlines;
        }
    }
    std::cout << "\e[" << nlines + 1 << "F        *[\a\x1b[32mEnd\x1b[0m]Build FEM Matrix\n";
    std::cout << "\e[" << nlines + 1 << "E";

    //SpMtx.debug();
}