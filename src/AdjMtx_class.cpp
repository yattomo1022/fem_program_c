#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include "AdjMtx_class.hpp"
#include "geometry_class.hpp"
#include <armadillo>

AdjMtx_class::AdjMtx_class(int nDOF)
{
  tree = arma::field<tree_class>(nDOF);
  Sp_index.reset(new SpMtx_core);
}

void AdjMtx_class::add_clique(const geometry_class &geo)
{
  int n_elem  = geo.get_nelem();
  int n_cList = 0;
  arma::Col<int> buf;
  arma::Col<int> buf_di;
  std::cout << "  +---- Creating adjacent list..." << '\n';

  for (int i = 0; i < n_elem; ++i)
  {
    //std::cout << geo.get_InterfaceInfo(i) << "\n";

    if (geo.get_InterfaceInfo(i) == -1)
    {
      //std::cout << "Direchlet境界のみをのぞいて確保\n";
      //buf.reset(); buf_di.reset();
      geo.get_MPC_cList(i, buf, buf_di);

      //std::cout << "List:" << buf.t() << "\n";
      //std::cout << "Dire:" << buf_di.t() << "\n";

      n_cList = buf.n_rows;
      for (int j = 0; j < n_cList; ++j)
      {
        tree(buf(j)).insert(buf);
      }
    }
  }
  std::cout << "\e[1F  +---- [\a\x1b[32mEnd\x1b[0m]Creating adjacent list" << '\n';
}

int AdjMtx_class::associate(const geometry_class &geo, std::unique_ptr<SpMtx_core> &SpMtx)
{
  int nnz      = 0;
  int max_deg1 = 0;
  int deg1_i   = 0;
  int nDOF     = geo.get_DOF();

  if (Sp_index)
  {

    for (int i = 0; i < nDOF; ++i)
    {
      deg1_i = tree(i).get_ntree();
      if (deg1_i == 0)
      {
        std::cout << " \a\x1b[31mERROR: \x1b[0mglobal DOF must be continuous. DOF No.->" << i << "\n";
        exit(1);
      }

      nnz += deg1_i;
      if (max_deg1 < deg1_i)
      {
        max_deg1 = deg1_i;
      }
    }

    //std::cout << "ind" << nDOF + 1<<"\n";
    //std::cout << "DOF" << nnz <<"\n";

    Sp_index->ind.reset(new long long[nDOF + 1]);
    Sp_index->DOF.reset(new long long[nnz]);
    arma::Col<int> buf(max_deg1);
    int p = 0;
    //int start = 0;
    Sp_index->ind[0] = p;

    for (int i = 0; i < nDOF; ++i)
    {

      deg1_i = tree(i).get_tree(buf);
      //start = 0;

      for (int j = 0; j < deg1_i; ++j)
      {
        //std::cout << i << " "<<p <<"\n";
        Sp_index->DOF[p] = buf(j);
        ++p;
      }
      Sp_index->ind[i + 1] = p;
    }

    tree.reset();
    SpMtx = std::move(Sp_index);
  }

  return nnz;
}
