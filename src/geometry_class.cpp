#include "geometry_class.hpp"
#include "geometry_format_class.hpp"
#include "gmsh_format_class.hpp"
#include "utility_tool_class.hpp"
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
using namespace std;

// geometry class
//コンストラクタ
geometry_class::geometry_class(string s)
{
  mesh_filename = s;
  DOF = 0;
}

// tag_typeのコンストラクタ
geometry_class::tag_type::tag_type()
{
  tag = 0;
  ID1 = 0;
  ID2 = 0;
  ID3 = 0;
}

// elem_Listのコンストラクタ
geometry_class::elem_type::elem_type()
{
  sID = 0;
  nnode = 0;
  PhysTag = 0;
  ID.fill(0);
}

// MPCコンストラクタ
geometry_class::MPC_type::MPC_type() {}

//==========================================================
//メソッド
//==========================================================

//----------------------------------------------------------
// make MPC table
//----------------------------------------------------------
void geometry_class::mk_MPC()
{
  // initialize
  std::cout << "  +---- Making MPC table for this FEM program...\n";
  int nlines = 0; // for CUI
  int n_elem = elemList.n_elem;

  int (geometry_class::*BFunc[])(int) = {&geometry_class::regist_AC,
                                         &geometry_class::regist_EM};

  // make DomainType
  int max_domain = 0;
  for (int i = 0; i < n_elem; ++i)
  {
    if (max_domain < elemList(i).ID(0))
    {
      max_domain = elemList(i).ID(0);
    }
  }

  MPC.DomainType = arma::Col<int>(max_domain);
  MPC.DomainType.fill(-1);
  MPC.Table = arma::Mat<int>(nodeList.n_cols, MPC.DomainType.n_rows, arma::fill::zeros);
  MPC.DomainelemDOF = arma::Row<int>(MPC.DomainType.n_rows, arma::fill::zeros);
  MPC.Domainnnode = arma::Row<int>(MPC.DomainType.n_rows, arma::fill::zeros);

  for (int i = 0; i < n_elem; ++i)
  {
    if (elemList(i).elemInfo(BodyType) != -1)
    {
      if (MPC.DomainType(elemList(i).ID(0) - 1) == -1 ||
          MPC.DomainType(elemList(i).ID(0) - 1) == elemList(i).elemInfo(BodyType))
      {
        MPC.DomainType(elemList(i).ID(0) - 1) = elemList(i).elemInfo(BodyType);
      }
      else
      {
        std::cout << "  \a\x1b[35mWarning: \x1b[0mCompatible domain type -> "
                  << MPC.DomainType(elemList(i).ID(0) - 1) << ", "
                  << elemList(i).elemInfo(BodyType) << "\n";
        ++nlines;
      }
    }
  }

  // check MPC table
  for (int i = 0; i < n_elem; ++i)
  {
    if (0 <= elemList(i).elemInfo(BodyType) &&
        elemList(i).elemInfo(BodyType) <= 1) //オーバーフロー対策
    {
      nlines += (this->*BFunc[elemList(i).elemInfo(BodyType)])(i);
    }
    else if (elemList(i).elemInfo(BodyType) == -1)
    {
    }
    else
    {
      std::cout << " \a\x1b[35mWarning: \x1b[0mUndifined domain type\n";
      ++nlines;
    }
  }

  // count nnode of each domains
  MPC.Domainnnode = sum(MPC.Table, 0);

  // std::cout << MPC.DomainDOF;

  // check MPC table(interface)
  int (geometry_class::*IFunc[])(int) = {&geometry_class::regist_BD_AC,
                                         &geometry_class::regist_IF_AC};
  for (int i = 0; i < n_elem; ++i)
  {
    if (0 <= elemList(i).elemInfo(InterfaceType) &&
        elemList(i).elemInfo(InterfaceType) <= 1)
    { //オーバーフロー対策
      nlines += (this->*IFunc[elemList(i).elemInfo(InterfaceType)])(i);
    }
    else if (elemList(i).elemInfo(InterfaceType) == -1)
    {
    }
    else
    {
      std::cout << " \a\x1b[35mWarning: \x1b[0mUndifined domain type\n";
      ++nlines;
    }
  }

  int id = 0;
  int nnode = nodeList.n_cols;
  int nDomain = MPC.DomainType.n_rows;
  int tmp;
  for (int i = 0; i < nDomain; ++i)
  {
    for (int j = 0; j < nnode; ++j)
    {
      if (MPC.Table(j, i) == 0)
      {
        MPC.Table(j, i) = -1; //書き換える。
      }
      else if (MPC.Table(j, i) < 0)
      {
        MPC.Table(j, i) = MPC.Table(j, -MPC.Table(j, i) - 1);
      }
      else
      {
        tmp = MPC.Table(j, i);
        MPC.Table(j, i) = id;
        id += tmp;
      }
    }
  }
  std::cout << "\e[" << nlines + 1 << "F  +---- [\a\x1b[32mEnd\x1b[0m]Making MPC table for this FEM program.\n";
  std::cout << "\e[" << nlines + 1 << "E";
  DOF = id;
  //std::cout << MPC.Table << '\n';
}

//----------------------------------------------------------
// make MPC table（subroutine）
//----------------------------------------------------------
// register function
int geometry_class::regist_AC(int i)
{
  int n_rows = elemList(i).cList.n_rows;
  int nlines = 0;
  for (int j = 0; j < n_rows; ++j)
  {
    if (MPC.DomainelemDOF(elemList(i).ID(0) - 1) == 0 ||
        MPC.DomainelemDOF(elemList(i).ID(0) - 1) == 1)
    {
      MPC.Table(elemList(i).cList(j), elemList(i).ID(0) - 1) = 1;
      MPC.DomainelemDOF(elemList(i).ID(0) - 1) = 1;
    }
    else
    {
      std::cout << "  \a\x1b[35mWarning: \x1b[0mProgram error in regist_AC. Please ask to program auther.\n";
    }
  }
  return nlines;
}

int geometry_class::regist_IF_AC(int i)
{
  int nlines = 0;
  int n_rows = elemList(i).cList.n_rows;
  for (int j = 0; j < n_rows; ++j)
  {
    if (MPC.Table(elemList(i).cList(j), elemList(i).ID(0) - 1) == 0 ||
        MPC.Table(elemList(i).cList(j), elemList(i).ID(1) - 1) == 0)
    {
      std::cout << " \a\x1b[35mWarning: \x1b[0mInvalid AC AC Interface\n";
      ++nlines;
    }
    else
    {
      if (elemList(i).ID(0) < elemList(i).ID(1))
      {
        MPC.Table(elemList(i).cList(j), elemList(i).ID(1) - 1) = -elemList(i).ID(0);
      }
      else if (elemList(i).ID(0) > elemList(i).ID(1))
      {
        MPC.Table(elemList(i).cList(j), elemList(i).ID(0) - 1) = -elemList(i).ID(1);
      }
      else
      {
        std::cout << " \a\x1b[35mWarning: \x1b[0mInvalid AC AC Interface\n";
        ++nlines;
      }
      // elemList(i).nDOF = 1;
    }
  }

  return nlines;
}

//----------------------------------------------------------
// parse direchlet
//----------------------------------------------------------
void geometry_class::parse_direchlet()
{
  std::cout << "  +---- Parsing Direchlet boundaries...\n";
  void (geometry_class::*DFunc[])(int) = {&geometry_class::regist_AC_direchlet};
  int n_elem = elemList.n_elem;

  DirechletTable = arma::Col<int>(DOF, arma::fill::zeros);

  for (int i = 0; i < n_elem; ++i)
  {
    if (0 <= elemList(i).elemInfo(DirechletType) &&
        elemList(i).elemInfo(DirechletType) <= 0)
    { //オーバーフロー対策
      (this->*DFunc[elemList(i).elemInfo(DirechletType)])(i);
    }
  }
  // std::cout << DirechletTable << "\n";
  std::cout
      << "\e[1F  +---- [\a\x1b[32mEnd\x1b[0m]Parsing Direchlet boundaries.\n";
}

void geometry_class::regist_AC_direchlet(int i)
{
  int n_rows = elemList(i).cList.n_rows;
  for (int j; j < n_rows; ++j)
  {
    DirechletTable(elemList(i).cList(j)) = 1;
  }
}

//----------------------------------------------------------
// write geometry information
//----------------------------------------------------------
void geometry_class::write_geoinfo()
{
  std::cout << "  +---- geometry infomation\n";
  std::cout << "        /=========/=======/=======/=======/=======/=======/\n";
  std::cout << "        |      No.|      1|      2|      3|      4|      5|\n";
  std::cout << "        +---------+-------+-------+-------+-------+-------+\n";
  std::cout << "        |     Type|";
  int nDomain = MPC.DomainType.n_rows;
  for (int i = 0; i < nDomain; ++i)
  {
    switch (MPC.DomainType(i))
    {
    case -1:
      std::cout << "   None|";
      break;
    case 0: // AC
      std::cout << "Acoust.|";
      break;
    default:
      std::cout << "<ERROR>|";
      break;
    }
  }
  std::cout << "\n";
  std::cout << "        |      DOF|";

  for (int i = 0; i < nDomain; ++i)
  {
    std::cout << std::setw(7) << MPC.Domainnnode(i) * MPC.DomainelemDOF(i) << "|";
  }
  std::cout << "\n";
  std::cout << "        +---------+-------+-------+-------+-------+-------+\n";
  std::cout << "        | Number of DOF in this system: " << std::setw(18) //左寄せ
            << std::left << DOF << "|\n";
  std::cout << "        /=========/=======/=======/=======/=======/=======/\n";
}

//----------------------------------------------------------
// read gmsh (gmsh version2.2仕様)
//----------------------------------------------------------

void geometry_class::read_gmsh()
{
  std::ifstream ifs; //file操作関連
  std::stringstream ss;

  //------------------file open---------------------
  ifs.open(mesh_filename);

  if (ifs.fail())
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mFailed to open file.\n";
    ifs.close();
    exit(1);
  }
  else
  {
    std::cout << "  +---- Reading " << mesh_filename << "...\n";
  }

  string stmp;
  int num, itmp;
  double dtmp;
  char ctmp[64];
  //--------------4行読み飛ばし---------------------
  getline(ifs, stmp); // std::cout << stmp << '\n';
  getline(ifs, stmp); // std::cout << stmp << '\n';
  getline(ifs, stmp); // std::cout << stmp << '\n';
  getline(ifs, stmp); // std::cout << stmp << '\n';

  //--------------tagname read---------------------
  getline(ifs, stmp);
  sscanf(stmp.data(), "%d", &num); // std::cout << num << '\n';
  tagList = arma::field<tag_type>(num);

  for (int i = 0; i < tagList.n_elem; i++)
  {
    getline(ifs, stmp);
    sscanf(stmp.data(), "%*d %d \" %s %d %d %d \" ", &itmp, ctmp,
           &tagList(i).ID1, &tagList(i).ID2,
           &tagList(i).ID3); // std::cout << stmp << '\n';

    tagList(itmp - 1).tagname =
        utility_tool_class::StrToLower(ctmp); //大文字小文字変換
  }

  //--------------2行読み飛ばし---------------------
  getline(ifs, stmp); // std::cout << stmp << '\n';
  getline(ifs, stmp); // std::cout << stmp << '\n';

  //--------------Node List---------------------
  getline(ifs, stmp);
  sscanf(stmp.data(), "%d", &num); // std::cout << num << '\n';
  nodeList = arma::Mat<double>(3, num);

  for (int i = 0; i < nodeList.n_cols; i++)
  {
    for (int j = 0; j < 4; j++)
    {
      ifs >> dtmp;
      if (j != 0)
      { // 0読み飛ばし
        nodeList(j - 1, i) = dtmp;
      }
    }
  }

  getline(ifs, stmp); // std::cout << stmp << '\n'; //最後に残った改行コード

  //--------------2行読み飛ばし---------------------
  getline(ifs, stmp); // std::cout << stmp << '\n';
  getline(ifs, stmp); // std::cout << stmp << '\n';

  //--------------Elem List---------------------
  getline(ifs, stmp);
  sscanf(stmp.data(), "%d", &num); // std::cout << "nelem: " <<num << '\n';
  elemList = arma::field<elem_type>(num);

  for (int i = 0; i < elemList.n_elem; i++)
  {
    ifs >> itmp; //読み捨て
    ifs >> itmp; // sID
    elemList(i).sID = itmp;
    // std::cout <<    elemList[i].sID << '\n';

    ifs >> itmp; // ntag
    // allocate
    elemList(i).itag = arma::Col<int>(itmp);
    elemList(i).nnode = gmsh_format_class::get_nv(elemList(i).sID);
    elemList(i).cList = arma::Col<int>(elemList(i).nnode);

    for (int j = 0; j < elemList(i).itag.n_elem; j++)
    {
      ifs >> itmp;
      elemList(i).itag(j) = itmp - 1; // 0-based array
    }

    for (int j = 0; j < elemList(i).nnode; j++)
    {
      ifs >> itmp;
      elemList(i).cList(j) = itmp - 1; // 0-based array
    }
  }

  getline(ifs, stmp); // std::cout << stmp << '\n'; //最後に残った改行コード
  getline(ifs, stmp); //正常ならここまで

  if (ifs.fail())
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mFailed to read file.\n";
    ifs.close();
    exit(1);
  }

  getline(ifs, stmp); //正常ならここは失敗する
  if (!ifs.fail())
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mFailed to read file.\n";
    ifs.close();
    exit(1);
  }

  std::cout << "\e[1F  +---- [\a\x1b[32mEnd\x1b[0m]Reading " << mesh_filename
            << ".\n";
  ifs.close();

  //-------------tagname to Phystag---------------------
  //-------------register elemList's ID---------------------
  std::cout << "  +---- Converting tagname to Phystag of this FEM program...\n";

  for (int i = 0; i < elemList.n_elem; i++)
  {
    elemList(i).PhysTag = geometry_format_class::get_Phystag(
        tagList(elemList(i).itag(0)).tagname);
    elemList(i).elemInfo = geometry_format_class::get_elemInfo(
        tagList(elemList(i).itag(0)).tagname);
    // std::cout << elemList(i).elemInfo << "\n";

    elemList(i).ID(0) = tagList(elemList(i).itag(0)).ID1;
    elemList(i).ID(1) = tagList(elemList(i).itag(0)).ID2;
    elemList(i).ID(2) = tagList(elemList(i).itag(0)).ID3;
  }

  std::cout << "\e[1F  +---- [\a\x1b[32mEnd\x1b[0m]Converting tagname to "
               "Phystag in this FEM program.\n";
}

//----------------------------------------------------------
// getter
//----------------------------------------------------------

int geometry_class::get_nelem() const { return elemList.n_elem; }

int geometry_class::get_nnode() const { return nodeList.n_cols; }

int geometry_class::get_DOF() const { return DOF; }

int geometry_class::get_InterfaceInfo(int i) const
{
  // std::cout << "This is here" << "\n";
  return elemList(i).elemInfo(1);
}

void geometry_class::get_MPC_cList(int i, arma::Col<int> &buf, arma::Col<int> &buf_di) const
{
  int domainNo = elemList(i).ID(0) - 1;
  int cnt = 0;
  int cnt_di = 0;
  int ncList = elemList(i).cList.n_rows;
  int nDOF = MPC.DomainelemDOF(elemList(i).ID(0) - 1);
  for (int j = 0; j < ncList; ++j)
  {
    for (int k = 0; k < nDOF; ++k)
    {
      if (DirechletTable(MPC.Table(elemList(i).cList(j) + k, domainNo)) == 0)
      {
        ++cnt;
      }
      else
      {
        ++cnt_di;
      }
    }
  }

  // std::cout << "1\n";
  /*   arma::Col<int> buf;
  arma::Col<int> buf_di; */
  if (cnt == 0)
  {
    buf.reset();
  }
  else
  {
    buf = arma::Col<int>(cnt);
  }
  if (cnt_di == 0)
  {
    buf_di.reset();
  }
  else
  {
    buf_di = arma::Col<int>(cnt_di);
  }

  cnt = 0;
  cnt_di = 0;
  for (int j = 0; j < ncList; ++j)
  {
    if (DirechletTable(elemList(i).cList(j)) == 0)
    {
      buf(cnt) = MPC.Table(elemList(i).cList(j), domainNo);
      ++cnt;
    }
    else
    {
      buf_di(cnt_di) = MPC.Table(elemList(i).cList(j), domainNo);
      ++cnt_di;
    }
  }
}

void geometry_class::get_cList(int i, arma::Col<int> &cList) const
{
  cList = elemList(i).cList;
}

int geometry_class::get_elemDOF(int i) const
{
  return MPC.DomainelemDOF(elemList(i).ID(0) - 1);
}

void geometry_class::get_elemnodeList(arma::mat &elemnodeList, int i) const
{
  int ncList = elemList(i).cList.n_rows;
  elemnodeList = arma::mat(3, ncList);
  for (int j = 0; j < ncList; ++j)
  {
    elemnodeList.col(j) = nodeList.col(elemList(i).cList(j));
  }
}

std::tuple<int, int> geometry_class::get_sID_PhysTag(int i) const
{
  return std::tie(elemList(i).sID, elemList(i).PhysTag);
}

int geometry_class::get_elemMPC_Col(int i) const { return elemList(i).ID(0); }

arma::Col<int> geometry_class::get_DomainType() const { return MPC.DomainType; }

void geometry_class::get_reconst(arma::Col<int> &nodeID, arma::Col<int> &elemID,
                                 arma::Col<int> &G2LTable,
                                 int domainID) const // int is domainID
{
  int nnode = nodeList.n_cols;
  int nodecnt = 0;

  for (int j = 0; j < nnode; ++j)
  {
    if (MPC.Table(j, domainID - 1) != -1)
    {
      ++nodecnt;
    }
  }

  if (nodecnt != 0)
  {
    nodeID = arma::Col<int>(nodecnt, arma::fill::zeros);
    G2LTable = arma::Col<int>(nnode);
    G2LTable.fill(-1);
    nodecnt = 0;
    for (int j = 0; j < nnode; ++j)
    {
      if (MPC.Table(j, domainID - 1) != -1)
      {
        nodeID(nodecnt) = j;
        G2LTable(j) = nodecnt;
        ++nodecnt;
      }
    }
  }

  int nelem = elemList.n_elem;
  int elemcnt = 0;
  for (int j = 0; j < nelem; ++j)
  {
    if (elemList(j).ID(0) == domainID)
    {
      ++elemcnt;
    }
  }
  elemID = arma::Col<int>(elemcnt, arma::fill::zeros);
  elemcnt = 0;
  for (int j = 0; j < nelem; ++j)
  {
    if (elemList(j).ID(0) == domainID)
    {
      elemID(elemcnt) = j;
      ++elemcnt;
    }
  }
}

arma::Col<double> geometry_class::get_node(int i) const
{
  return nodeList.col(i);
}

int geometry_class::get_MPCgid(int i, int j) const { return MPC.Table(i, j - 1); }
