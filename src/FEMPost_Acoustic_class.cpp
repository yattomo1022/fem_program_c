#include "FEMPost_Acoustic_class.hpp"
#include "FEMPost_Core.hpp"
#include "geometry_class.hpp"
#include "main.hpp"
#include "vtkOutput_class.hpp"
#include <fstream>
#include <iostream>
#include <string>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <memory>
#define complex std::complex<double>

const int FEMPost_Acoustic_class::max_vtk_output = 1;
const arma::field<std::string> FEMPost_Acoustic_class::dataname = {"SPL"};
const arma::Col<int> FEMPost_Acoustic_class::datatype = {0};

FEMPost_Acoustic_class::FEMPost_Acoustic_class(int a) : FEMPost_Core(a)
{
  std::cout << "  +---- FEM post process for Acoustic is initialized.\n";
}

void FEMPost_Acoustic_class::reconst_sol(const geometry_class &geo, const std::unique_ptr<complex[]> &sol)
{
  int nnode = nodeID.n_rows;
  pressure = arma::Col<complex>(nnode);

  for (int i = 0; i < nnode; ++i)
  {
    pressure(i) = sol[geo.get_MPCgid(nodeID(i), domainID)];
  }
  std::cout << "        *Reconstruct solution for Acoustics.\n";
}

void FEMPost_Acoustic_class::VTK_output(const geometry_class &geo, int freq)
{
  //
  vtk_flag = arma::Col<int>(max_vtk_output, arma::fill::zeros);
  vtk_flag(0) = 1;
  arma::Mat<double> *ptr[max_vtk_output];
  void (FEMPost_Acoustic_class::*PFunc[])(void) = {&FEMPost_Acoustic_class::calc_SPL};
  for (int i = 0; i < max_vtk_output; ++i)
  {
    if (vtk_flag(i) == 1)
    {
      (this->*PFunc[i])();
      ptr[i] = &SPL;
    }
    else
    {
      ptr[i] = nullptr;
    }
  }

  ofstream fileid;
  string filename = ::vtkdir + "test" + "_" + "Domain" + std::to_string(domainID) + "_" + "Freq" + std::to_string((int)freq) + ".vtu";
  vtkOutput_class::vtkOutput_header(fileid, filename, nodeID.n_rows, elemID.n_rows);
  vtkOutput_class::vtkOutput_pointdata(fileid, datatype, dataname, ptr);
  vtkOutput_class::vtkOutput_point(fileid, geo, nodeID);
  vtkOutput_class::vtkOutput_cell(fileid, geo, elemID, G2LTable);
  vtkOutput_class::vktOutput_footer(fileid);
}

// void FEMPost_Acoustic_class::init_intpl(const geometry_class &geo, const arma::Mat<double> &recarray, arma::Col<int> found_flag)
// {
// }

void FEMPost_Acoustic_class::calc_SPL()
{
  if (pressure.n_elem == 0)
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mFEMPost_Acoustic Can't calculate SPL because sound pressure is not defined.\n";
    exit(1);
  }

  SPL = 20.0 * arma::log10(arma::abs(pressure));
}