#include "gmsh_format_class.hpp"
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

arma::Mat<int> gmsh_format_class::Table;

void gmsh_format_class::init()
{
    Table = arma::Mat<int>(137, 2);
    //* 1st Order Elements
    // 2-node Line
    Table(0, 0) = 2; //Number of Point
    Table(0, 1) = 1; //Dimension
    // 3-node Triangle
    Table(1, 0) = 3;
    Table(1, 1) = 2;
    // 4-node Quadrangle
    Table(2, 0) = 4;
    Table(2, 1) = 2;
    // 4-node Tetrahedron
    Table(3, 0) = 4;
    Table(3, 1) = 3;
    // 8-node Hexahedron
    Table(4, 0) = 8;
    Table(4, 1) = 3;
    // 6-node Prism
    Table(5, 0) = 6;
    Table(5, 1) = 3;
    // 5-node Pyramid
    Table(6, 0) = 5;
    Table(6, 1) = 3;

    //* 2nd Order Elements
    // 3-node Line
    Table(7, 0) = 3;
    Table(7, 1) = 1;
    // 6-node Triangle
    Table(8, 0) = 6;
    Table(8, 1) = 2;
    // 9-node Quadrangle (w/ center node)
    Table(9, 0) = 9;
    Table(9, 1) = 2;
    // 10-node Tetrahedron
    Table(10, 0) = 10;
    Table(10, 1) = 3;
    // 27-node Hexahedron
    Table(11, 0) = 27;
    Table(11, 1) = 3;
    // 18-node Prism
    Table(12, 0) = 18;
    Table(12, 1) = 3;
    // 14-node Pyramid
    Table(13, 0) = 14;
    Table(13, 1) = 3;

    //* Point
    // 1-node Point
    Table(14, 0) = 1;
    Table(14, 1) = 0;

    //* Serendipity 2nd Order Elements (w/o center node)
    // 8-node Quadrangle
    Table(15, 0) = 8;
    Table(15, 1) = 2;
    // 20-node Hexahedron
    Table(16, 0) = 20;
    Table(16, 1) = 3;
    // 15-node Prism
    Table(17, 0) = 15;
    Table(17, 1) = 3;
    // 13-node Pyramid
    Table(18, 0) = 13;
    Table(18, 1) = 3;

    //* Other Higher Order Elements
    // 9-node 3rd Order Triangle (w/o center node)
    Table(19, 0) = 9;
    Table(19, 1) = 2;
    // 10-node 3rd Order Triangle (w/ center node)
    Table(20, 0) = 10;
    Table(20, 1) = 2;
    // 12-node 4th Order Incomplete Triangle (w/o center node)
    Table(21, 0) = 12;
    Table(21, 1) = 2;
    // 15-node 4th Order Triangle (w/ 3 center nodes)
    Table(22, 0) = 15;
    Table(22, 1) = 2;
    // 15-node 5th Order Incomplete Triangle (w/o center node)
    Table(23, 0) = 15;
    Table(23, 1) = 2;
    // 21-node 5th Order Complete Triangle (w/ 6 center nodes)
    Table(24, 0) = 21;
    Table(24, 1) = 2;

    // 4-node 3rd Order Line
    Table(25, 0) = 4;
    Table(25, 1) = 1;
    // 5-node 4th Order Line
    Table(26, 0) = 5;
    Table(26, 1) = 1;
    // 6-node 5th Order Line
    Table(27, 0) = 6;
    Table(27, 1) = 1;

    // 20-node 3rd Order Tetrahedron
    Table(28, 0) = 20;
    Table(28, 1) = 3;
    // 35-node 4th Order Tetrahedron
    Table(29, 0) = 35;
    Table(29, 1) = 3;
    // 56-node 5th Order Tetrahedron
    Table(30, 0) = 56;
    Table(30, 1) = 3;
    // 22-node Tetrahedron (unknown detail)
    Table(31, 0) = 22;
    Table(31, 1) = 3;
    // 28-node Tetrahedron (unknown detail)
    Table(32, 0) = 28;
    Table(32, 1) = 3;
    // Unknown
    Table(33, 0) = -1;
    Table(33, 1) = -1;
    // Unknown
    Table(34, 0) = -1;
    Table(34, 1) = -1;

    // 16-node 3rd Order Quadrangle
    Table(35, 0) = 16;
    Table(35, 1) = 2;
    // 25-node 4th Order Quadrangle
    Table(36, 0) = 25;
    Table(36, 1) = 2;
    // 36-node 5th Order Quadrangle
    Table(37, 0) = 36;
    Table(37, 1) = 2;
    // 12 node Quadrangle (unknown detail)
    Table(38, 0) = 12;
    Table(38, 1) = 2;
    // 16-node Incomplete Quadrangle (unknown detail)
    Table(39, 0) = 16;
    Table(39, 1) = 2;
    // 20-node Quadrangle (unknown detail)
    Table(40, 0) = 20;
    Table(40, 1) = 2;
}

int gmsh_format_class::get_nv(const int sID)
{
    return Table(sID - 1, 0);
}

int gmsh_format_class::get_nd(const int sID)
{
    return Table(sID - 1, 1);
}