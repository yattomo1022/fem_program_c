
#include "AdjMtx_class.hpp"
#include "FEMPost_Acoustic_class.hpp"
#include "FEMPost_Core.hpp"
#include "SpMtx_class.hpp"
#include "buildFEMMtx_class.hpp"
#include "geometry_class.hpp"
#include "geometry_format_class.hpp"
#include "gmsh2vtk_class.hpp"
#include "gmsh_format_class.hpp"
#include "parameter_class.hpp"
#include "shapefunc_class.hpp"
#include "time_class.hpp"
#include "utility_tool_class.hpp"
#include "main.hpp"
#include <iostream>
#include <string>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <complex>
#define complex std::complex<double>
using namespace std;

//グローバル変数
std::string gmshdir = "./1GmshFile/";
std::string vtkdir = "./2VTKFile/";
const double PI = 3.14159265358979323846;
const complex ci = complex(0.0, 1.0);

int main()
{

  time_class time;

  //----------start program--------------
  std::cout << "****************************************************\n";
  std::cout << "* Start Numerical Acoustics: FEM_Program C/C++ ver  \n";
  std::cout << "* Start Time: ";
  time.get_time(); //Get Current Time
  std::cout << "*---------------------------------------------------\n";
  std::cout << '\n';

  //----------initial settings-----------
  gmsh2vtk_class::init();
  geometry_format_class::init();
  gmsh_format_class::init();

  //----------load geometry file-------------
  string filename = gmshdir + "RectQ_IF.msh";
  geometry_class geo(filename);

  std::cout << "o-+-- Setting geometry \n";
  geo.read_gmsh();
  geo.mk_MPC();
  geo.parse_direchlet();
  geo.write_geoinfo();

  //----------load parameter-------------
  std::cout << '\n';
  std::cout << "o-+-- Setting parameter \n";
  parameter_class param;
  param.init(geo);
  param.set_air_param(1, 20.0);
  //param.set_air_param(3); //省略した場合20度になる
  param.set_default(); //write this in the last

  //----------load other settings------------
  arma::Col<double> freq = {62.5, 125.0};
  int nfreq = freq.n_rows;

  //----------set for PostProcess------------
  std::cout << '\n';
  std::cout << "o-+-- Post Process setting\n";
  arma::Col<int> DomainType = geo.get_DomainType();
  int nDomainType = DomainType.n_rows;
  arma::field<unique_ptr<FEMPost_Core>> FEMPost(nDomainType);
  for (int i = 0; i < nDomainType; ++i)
  {
    switch (DomainType(i))
    {
    case -1:
      FEMPost(i).reset(new FEMPost_dummy_class(i + 1)); //1 based domainID array
      FEMPost(i)->init(geo);
      break;
    case 0:
      FEMPost(i).reset(new FEMPost_Acoustic_class(i + 1)); //1 based domainID array
      FEMPost(i)->init(geo);
      break;
    default:
      std::cout << " \a\x1b[31mERROR: \x1b[0mmain\n";
      exit(1);
      break;
    }
  }

  //----------make shape function-------------
  std::cout << '\n';
  std::cout << "o---- Shape Func. setting\n";
  arma::field<unique_ptr<shapefunc_class_Core>> SF(4);
  //節点数，積分点数，sID，次元（積分点数以外を変えるようにはできてない）
  SF(0).reset(new SF_L_O1(2, 2, 1, 1));
  SF(0)->set_SF();
  SF(1).reset(new SF_Tr_O1(3, 4, 2, 2));
  SF(1)->set_SF();
  SF(2).reset(new SF_Qu_O1(4, 4, 3, 2));
  SF(2)->set_SF();
  SF(3).reset(new SF_Tet_O1(4, 4, 4, 3));
  SF(3)->set_SF();

  //----------make adjacent list-------------
  std::cout << '\n';
  std::cout << "o-+-- Parse for FEM matrix\n";

  AdjMtx_class AdjMtx(geo.get_DOF());
  SpMtx_class SpMtx;
  AdjMtx.add_clique(geo);
  SpMtx.init(geo, AdjMtx);

  //----------MAIN PROCESS-------------
  std::cout << '\n';
  std::cout << "o-+-- Main process(caclurating...)\n";
  std::unique_ptr<complex[]> sol;
  //buildFEMMtx_class FEMbuild; //静的クラスに変更_202010
  //FEMPost_class FEMPost; //静的クラス
  int flag_pardiso_final = 0;

  for (int i = 0; i < nfreq; ++i)
  {
    std::cout << "  +---- " << freq(i) << "[Hz]\n";
    SpMtx.init(geo, AdjMtx);
    //FEMbuild.BuildFEM_Mtx(geo, SF, SpMtx, param, freq(i)); //静的クラスに変更_202010
    buildFEMMtx_class::BuildFEM_Mtx(geo, SF, SpMtx, param, freq(i));
    if (i == nfreq - 1)
    {
      flag_pardiso_final = 1;
    }
    SpMtx.solve(sol, flag_pardiso_final);

    //----------POST PROCESS-------------
    for (int j = 0; j < nDomainType; ++j)
    {
      FEMPost(j)->reconst_sol(geo, sol);
      FEMPost(j)->VTK_output(geo, freq(i));
    }
  }

  //----------end program--------------
  std::cout << "*---------------------------------------------------\n";
  std::cout << "* End Numerical Acoustics: FEM_Program C/C++ ver  \n";
  std::cout << "* End Time: ";
  time.get_time();
  std::cout << "****************************************************\n";
}
