#include "parameter_class.hpp"
#include "geometry_class.hpp"
#include <iostream>
#include <memory>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>

void parameter_class::init(const geometry_class &geo)
{
  arma::Col<int> DomainType = geo.get_DomainType();
  int nDomain = DomainType.n_rows;
  param_conv = arma::Col<int>(nDomain);
  param_conv.fill(-1);
  int AC = 0;
  int EM = 0;

  for (int i = 0; i < nDomain; ++i)
  {
    switch (DomainType(i))
    {
    case -1:
      std::cout << " \a\x1b[34mRemark: \x1b[0m:Domain type is not set in Domain No." << i + 1 << ".\n";
      break;
    case 0:
      param_conv(i) = AC;
      ++AC;
      break;
    case 1:
      param_conv(i) = EM;
      ++EM;
      break;
    default:
      std::cout << " \a\x1b[31mERROR: \x1b[0mParameter_class:\n";
      exit(1);
      break;
    }
  }
  air_param = arma::field<air_type>(AC);

  //std::cout << param_conv << "\n";
}

void parameter_class::set_default()
{
  int nAC = air_param.n_elem;

  for (int i = 0; i < nAC; ++i)
  {
    if (!air_param(i).is_set)
    {
      set_air_param(i, 20.0, false);
      std::cout << "  +-- Default parameter is set in Acoustic Domain " << i + 1 << "\n";
    }
  }
}

void parameter_class::set_air_param(int domainID, double temp, bool flag)
{
  int k;
  if (flag) //全体の通し番号のDomainIDか，各種類毎の番号か
  {
    if (param_conv(domainID - 1) == -1)
    {
      std::cout << " \a\x1b[31mERROR: \x1b[0mThis domain can't be set parameters.\n";
      exit(1);
    }
    k = param_conv(domainID - 1);
  }
  else
  {
    k = domainID;
  }

  air_param(k).P0 = 101325;
  double atm = air_param(k).P0 * 9.86923 * 0.000001; //1気圧
  double rho_ref = 1.293;
  double Kelvin = temp + 273.2; //temperature in Kelvin

  air_param(k).rho0 = rho_ref * atm * 273.2 / Kelvin;
  air_param(k).c0 = 331.5 * std::sqrt(1.0 + temp / 273.2);
  air_param(k).Z_char = air_param(k).rho0 * air_param(k).c0;
  air_param(k).prandtl = 0.71;
  air_param(k).SHR = 1.4;
  air_param(k).viscosity = (1.724 * 0.00001) * (273.2 + 118.) / (273.2 + 118. + temp) * std::pow(Kelvin / 273.2, 1.5);

  air_param(k).c_T = (air_param(k).SHR - 1.0) / std::pow(air_param(k).c0, 2.0) * std::sqrt(air_param(k).viscosity / air_param(k).rho0 / air_param(k).prandtl);
  air_param(k).c_V = std::sqrt(air_param(k).viscosity / air_param(k).rho0);
  air_param(k).is_set = true;
}

double parameter_class::get_air_c0(int domainID) const
{
  if (param_conv(domainID - 1) == -1)
  {
    std::cout << " \a\x1b[31mERROR: \x1b[0mThis domain is valid for parameter.\n";
    exit(1);
  }
  return air_param(param_conv(domainID - 1)).c0;
}