#include <iostream>
#define ARMA_ALLOW_FAKE_CLANG
#define ARMA_ALLOW_FAKE_GCC
#include <armadillo>
#include <tree_class.hpp>

//コンストラクタ
tree_class::tree_class()
{
  root = nullptr;
  nVTX = 0;
}

tree_class::tree_node::tree_node()
{
  value = 0;
  L     = nullptr;
  R     = nullptr;
}

//デストラクタ
tree_class::tree_node::~tree_node()
{
  //std::cout << "tree_node is deleted\n";
}

tree_class::~tree_class()
{
  //std::cout << "tree is deleted\n";
}

//メンバ関数
void tree_class::insert(const arma::Col<int> &buf)
{
  int n;          //挿入するデータ数
  int j, st, sgn; //シャッフル用
  std::shared_ptr<tree_node> ptr;
  int a;

  n = buf.n_rows;
  //std::cout << n << "\n";

  j   = (n + 1) / 2;
  st  = 1;
  sgn = 1;

  //std::cout << j << "\n";

  if (root == nullptr)
  {

    root.reset(new tree_node);
    root->value = buf(j - 1);
    nVTX        = 1;

    sgn = -sgn;
    j   = j + sgn * (st - 1);
    st++;
  }

  for (int i = st; i < n + 1; ++i)
  {

    sgn = -sgn;
    j   = j + sgn * (i - 1);
    a   = buf(j - 1);
    //std::cout << a << "\n";

    ptr = root;

    while (true)
    {

      if (a == ptr->value)
      {
        break;
      }
      else if (a < ptr->value)
      {

        if (ptr->L != nullptr)
        {
          ptr = ptr->L;
        }
        else
        {
          ptr->L.reset(new tree_node);
          ptr        = ptr->L;
          ptr->value = a;
          nVTX++;
          break;
        }
      }
      else
      {

        if (ptr->R != nullptr)
        {
          ptr = ptr->R;
        }
        else
        {
          ptr->R.reset(new tree_node);
          ptr        = ptr->R;
          ptr->value = a;
          nVTX++;
          break;
        }
      }
    }
  }
  //std::cout << "nVTX: " << nVTX << "\n";
}

int tree_class::get_tree(arma::Col<int> &buf)
{
  int sp;
  arma::field<std::shared_ptr<tree_node>> stack(nVTX + 1);
  buf = arma::Col<int>(nVTX, arma::fill::zeros);

  if (root != nullptr)
  {
    sp        = 0;
    stack(sp) = root;

    for (int cnt = 0; cnt < nVTX; cnt++)
    {
      while (stack(sp) != nullptr)
      {
        stack(sp + 1) = stack(sp)->L;
        sp            = sp + 1;
      }
      sp        = sp - 1;
      buf(cnt)  = stack(sp)->value;
      stack(sp) = stack(sp)->R;
    }
  }

  return nVTX;
}

int tree_class::get_ntree()
{
  return nVTX;
}
